#include <stdio.h>

int count_occurrences(char *str, char ch) {
    int count = 0;
    for (int i = 0; str[i] != '\0'; i++) {
        if (str[i] == ch) {
            count++;
        }
    }
    return count;
}
int count_words(char *str, char *delimiter) {
    int count = 1; // On initialise à 1 pour prendre en compte le premier mot
    for (int i = 0; str[i] != '\0'; i++) {
        if (str[i] == delimiter[0] && str[i+1] != delimiter[0]) {
            count++;
        }
    }
    return count;
}

int count_words_better(char *str, char *delimiter) {
    int count = 0;
    int is_word = 0;
    for (int i = 0; str[i] != '\0'; i++) {
        if (str[i] != delimiter[0] && !is_word) {
            count++;
            is_word = 1;
        } else if (str[i] == delimiter[0]) {
            is_word = 0;
        }
    }
    return count;
}

int main() {
    char input_str[50];
    printf("Entrez une chaîne de caractères (maximum 49 caractères) :\n");
    scanf("%49s", input_str);
    printf("La chaîne saisie est : %s\n", input_str);

    char ch;
    printf("Entrez un caractère à chercher :\n");
    scanf(" %c", &ch);
    int count = count_occurrences(input_str, ch);
    printf("Le caractère %c apparaît %d fois dans la chaîne.\n", ch, count);

    int word_count = count_words(input_str, " ");
    printf("La chaîne contient %d mots.\n", word_count);

    int word_count_better = count_words_better(input_str, " ");
    printf("La chaîne contient %d mots (en prenant en compte les espaces consécutifs).\n", word_count_better);

    return 0;
}

