#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void small_to_zero(int* arr, int size, int val) {
    for (int i = 0; i < size; i=i+1) {
        if (arr[i] <= val) {
            arr[i] = 0;
        }
    }
}

void add_vectors(int size, double* arr1, double* arr2, long double* arr3) {
    for (int i = 0; i < size; i++) {
        arr3[i] = pow((arr1[i] - arr2[i]), 2);
    }
}

double norm_vector(int size, double* arr) {
    double sum = 0;
    for (int i = 0; i < size; i=i+1) {
        sum += pow(arr[i], 2);
    }
    return sqrt(sum);
}

int main() {
    int nums[] = { 1, 3, -1, 2, 7, 5, 12, 4, 4, 3, 0 };
    int size1 = sizeof(nums) / sizeof(nums[0]);

    printf("avant fonction small_to_zero:\n");
    for (int i = 0; i < size1; i=i+1) {
        printf("%d ", nums[i]);
    }
    printf("\n");

    small_to_zero(nums, size1, 3);

    printf("apres fonction small_to_zero:\n");
    for (int i = 0; i < size1; i=i+1) {
        printf("%d ", nums[i]);
    }
    printf("\n");

    double T1[] = { 3.14, -1.0, 2.3, 0, 7.1 };
    double T2[] = { 2.71, 2.5, -1, 3, -7 };
    long double T3[5];
    int size2 = sizeof(T1) / sizeof(T1[0]);

    printf("T1: ");
    for (int i = 0; i < size2; i=i+1) {
        printf("%+.2lf ", T1[i]);
    }
    printf("\nT2: ");
    for (int i = 0; i < size2; i=i+1) {
        printf("%+.2lf ", T2[i]);
    }
    printf("\n");

    add_vectors(size2, T1, T2, T3);

    printf("T3: ");
    for (int i = 0; i < size2; i=i+1) {
        printf("%.2Lf ", T3[i]);
    }
    printf("\n");

    double V[] = { 3.0, 4.0 };
    int size3 = sizeof(V) / sizeof(V[0]);

    printf("V: ");
    for (int i = 0; i < size3; i=i+1) {
        printf("%.2lf ", V[i]);
    }
    printf("\nNorm of V: %.2lf\n", norm_vector(size3, V));

    return 0;
}
